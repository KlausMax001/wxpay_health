package com.wang.health.web;

import com.wang.health.service.MemberService;
import com.wang.health.vo.Result;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/member")
public class MemberController {

    @DubboReference
    private MemberService memberService;

    @PostMapping("/findMemberByDate")
    public Result findMemberByDate(@RequestBody List<Date> dateList){
        Map map = memberService.findMemberByDate(dateList);
        return Result.build(map);
    }
}
