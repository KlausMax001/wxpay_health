package com.wang.health.auth;

import com.wang.health.Do.UserDo;
import com.wang.health.cst.SysConsts;
import com.wang.health.utils.RedisUtil;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class MyUserDetailsService implements UserDetailsService {
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserDo userDo = RedisUtil.get(SysConsts.REDIS_USER_PREFIX + username);
        if(userDo != null){
            List<String> roleCodeList = new ArrayList<>();
            userDo.getRoleList().stream().forEach(role -> {
                roleCodeList.add(role.getRoleCode());
            });
            String[] roleArray = new String[roleCodeList.size()];
        return new User(username,userDo.getPassword(),AuthorityUtils.createAuthorityList(roleCodeList.toArray(roleArray)));
        }
        return null;
    }
}