package com.wang.health.web;

import com.wang.health.service.OrderSettingService;
import com.wang.health.utils.POIUtil;
import com.wang.health.vo.Result;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/ordersetting")
public class OrderSettingController {

    @DubboReference
    private OrderSettingService orderSettingService;

    @PostMapping("/importData")
    public Result<Boolean> importData(@RequestParam("file") MultipartFile file) throws Exception {
        List<String[]> list = POIUtil.readExcel(file.getInputStream(), file.getOriginalFilename());
        orderSettingService.importData(list);
        return Result.build(true);
    }

    @GetMapping("/findByYearMonth/{yearMonth}")
    public Result findByYearMonth(@PathVariable String yearMonth){
        Map map = orderSettingService.findByYearMonth(yearMonth);
        return Result.build(map);
    }
    @GetMapping("/updateByDate/{date}/{number}")
    public Result updateByDate(@PathVariable("date") String date,@PathVariable("number") Integer number){
        orderSettingService.updateByDate(date,number);
        return Result.build(true);
    }
    /**
     * 测试清理1年前预约日程的方法是否正常。
     * @return
     */
    @GetMapping("/deleteOrderDate")
    public boolean deleteOrderDate(){
        return orderSettingService.clearOrderDate();
    }
}
