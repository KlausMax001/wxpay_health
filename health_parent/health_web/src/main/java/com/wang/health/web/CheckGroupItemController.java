package com.wang.health.web;

import com.wang.health.service.CheckGroupItemService;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/checkgroupitem")
public class CheckGroupItemController {

    @DubboReference
    private CheckGroupItemService checkGroupItemService;

    @GetMapping("/findByGroupId/{groupId}")
    public List<Long> findByGroupId(@PathVariable("groupId") String groupId){
        return checkGroupItemService.findByGroupId(groupId);
    }
}
