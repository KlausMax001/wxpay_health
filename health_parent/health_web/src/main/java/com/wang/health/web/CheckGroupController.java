package com.wang.health.web;

import com.wang.health.pojo.CheckGroup;
import com.wang.health.service.CheckGroupService;
import com.wang.health.vo.CheckGroupVo;
import com.wang.health.vo.PageResult;
import com.wang.health.vo.QueryPageBean;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/checkgroup")
public class CheckGroupController {

    @DubboReference
    private CheckGroupService checkGroupService;

    @PostMapping("/save")
    public boolean save(@RequestBody @Valid CheckGroupVo checkGroupVo){
        return checkGroupService.saveGroup(checkGroupVo);
    }
    @PostMapping("/findByPage")
    public PageResult findByPage(@RequestBody QueryPageBean pageBean){
        return checkGroupService.findByPage(pageBean);
    }

    @GetMapping("/delete/{id}")
    public boolean delete(@PathVariable("id") String id){
        return checkGroupService.removeById(id);

    }
    @GetMapping("/findAll")
    public List<CheckGroup> findAll(){
        return checkGroupService.list();
    }
}
