package com.wang.health.exception;

import com.wang.health.vo.Result;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionInvoker {

    @ExceptionHandler
    public Result<String> invokerException(Exception e){
        String errorMsg = e.getMessage();
        if (e instanceof MethodArgumentNotValidException){
            errorMsg = ((MethodArgumentNotValidException) e)
                    .getBindingResult()
                    .getFieldErrors().get(0)
                    .getDefaultMessage();
        }else if (e.getMessage()!=null&&e.getMessage().length()>250){
            errorMsg = e.toString();
        }

        return Result.buildError(errorMsg);
    }
}
