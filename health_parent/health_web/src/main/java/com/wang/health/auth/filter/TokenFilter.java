package com.wang.health.auth.filter;

import com.wang.health.Do.UserDo;
import com.wang.health.cst.SysConsts;
import com.wang.health.utils.JwtTokenUtil;
import com.wang.health.utils.RedisUtil;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TokenFilter extends OncePerRequestFilter {

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        //对token进行解析
        String authorization = httpServletRequest.getHeader("Authorization")!=null ? httpServletRequest.getHeader("Authorization") : httpServletRequest.getParameter("token");
        String username = null;
        if (authorization != null) {
            try {
                username = JwtTokenUtil.parseToken(authorization);
            } catch (Exception e) {
                e.printStackTrace();
            }
            UserDo userDo = RedisUtil.get(SysConsts.REDIS_USER_PREFIX + username);
            List<String> roleCodeList = new ArrayList<>();
            if (userDo != null) {


                userDo.getRoleList().stream().forEach(role -> {
                    roleCodeList.add(role.getRoleCode());
                });
                String[] roleArray = new String[roleCodeList.size()];
                UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
                        username,
                        null,
                        AuthorityUtils.createAuthorityList(roleCodeList.toArray(roleArray))
                );
                SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
            }
        }
        //放行
        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }
}
