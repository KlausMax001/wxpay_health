package com.wang.health.config;


import com.wang.health.auth.filter.TokenFilter;
import com.wang.health.auth.handle.MyAccessDeniedHandler;
import com.wang.health.auth.handle.MyAuthenticationEntryPoint;
import com.wang.health.auth.handle.MyAuthenticationFailureHandler;
import com.wang.health.auth.handle.MyAuthenticationSuccessHandler;
import com.wang.health.utils.MD5Util;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Bean
    public PasswordEncoder passwordEncoder() {
        //使用自定义的MD5工具类进行加密
        return new PasswordEncoder() {
            @Override
            public String encode(CharSequence rawPassword) {
                return MD5Util.encode(String.valueOf(rawPassword));
            }

            @Override
            public boolean matches(CharSequence rawPassword, String encodedPassword) {
                return encode(rawPassword).equals(encodedPassword);
            }
        };

        //return new BCryptPasswordEncoder();
    }

    /**
     * 静态资源过滤
     *
     * @param web
     * @throws Exception
     */
    @Override
    public void configure(WebSecurity web) throws Exception {
        //忽略swagger的页面
        web.ignoring().mvcMatchers(
                "/swagger-ui.html",
                "/webjars/**",
                "/v2/**",
                "/swagger-resources/**");
    }
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.formLogin()
                .successHandler(new MyAuthenticationSuccessHandler())//认证成功的处理器
                .failureHandler(new MyAuthenticationFailureHandler())//认证失败的处理器。
                .and()
                .exceptionHandling()
                .accessDeniedHandler(new MyAccessDeniedHandler())//403无权限处理器
                .authenticationEntryPoint(new MyAuthenticationEntryPoint())//其他异常处理器
                .and()
                .authorizeRequests()
                .antMatchers("/login*").permitAll()//登陆白名单，对于login请求不拦截
                .anyRequest().authenticated()//其他地址需要登陆后才能访问
                .and()
                .csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);//禁用session
        //在验证用户名密码权限之前添加token过滤器，解析并验证token
        http.addFilterBefore(new TokenFilter(), UsernamePasswordAuthenticationFilter.class);
    }
}
