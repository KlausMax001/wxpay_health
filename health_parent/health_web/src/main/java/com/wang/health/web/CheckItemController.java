package com.wang.health.web;

import com.wang.health.pojo.CheckItem;
import com.wang.health.service.CheckItemService;
import com.wang.health.vo.PageResult;
import com.wang.health.vo.QueryPageBean;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/checkitem")
public class CheckItemController {

    @DubboReference
    private CheckItemService checkItemService;

    /**
     * 增加或者修改操作，如果传递的对象中有id值，则查询数据库，
     * 若数据库中有对应的数据，则进行修改，否则进行添加，若id值为null，则直接进行添加。
     * @param checkItem
     * @return
     */
    @PostMapping("/save")
    private boolean add(@RequestBody @Valid CheckItem checkItem){
        //if (checkItem.getRemark()!=null&&checkItem.getRemark().length()>100){
        //    return "2";
        //}
        //if (checkItem.getAttention()!=null&&checkItem.getAttention().length()>100){
        //    return "3";
        //}
        return checkItemService.saveOrUpdate(checkItem);
    }

    @GetMapping("/findAll")
    public List<CheckItem> findAll(){
        return checkItemService.list();
    }

    @PreAuthorize("hasAuthority('admin')")
    @PostMapping("/findByPage")
    public PageResult findByPage(@RequestBody QueryPageBean pageBean){
        return checkItemService.findByPage(pageBean);
    }

    @GetMapping("/delete/{id}")
    public boolean delete(@PathVariable("id") String id){
        return checkItemService.removeById(id);
    }
}
