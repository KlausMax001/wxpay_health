package com.wang.health.web;

import com.wang.health.pojo.SetmealGroup;
import com.wang.health.service.SetmealGroupService;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/setmealgroup")
public class SetmealGroupController {

    @DubboReference
    private SetmealGroupService setmealGroupService;

    @GetMapping("/findBySetmealId/{id}")
    public List<Long> findBySetmealId(@PathVariable("id") String id){
        return setmealGroupService.findBySetmealId(id);
    }

}
