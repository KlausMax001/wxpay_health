package com.wang.health.auth.handle;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wang.health.utils.JwtTokenUtil;
import com.wang.health.vo.Result;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class MyAuthenticationSuccessHandler implements AuthenticationSuccessHandler {
    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {
        //获取登陆对象
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();

        try {
            //根据用户名创建token
            String token = JwtTokenUtil.createToken(userDetails.getUsername());
            //创建返回结果
            Result<String> result = Result.build(token);
            //创建objectMapper用于将对象转换为json字符串返回给前端
            ObjectMapper objectMapper = new ObjectMapper();
            //将json字符串写回。
            httpServletResponse.getWriter().write(objectMapper.writeValueAsString(result));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
