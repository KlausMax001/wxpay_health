package com.wang.health.web;

import com.wang.health.service.SetmealService;
import com.wang.health.utils.AliyunFileUtil;
import com.wang.health.vo.PageResult;
import com.wang.health.vo.QueryPageBean;
import com.wang.health.vo.Result;
import com.wang.health.vo.SetmealVo;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("/setmeal")
public class SetmealController {

    @DubboReference
    private SetmealService setmealService;
    @Resource
    private RedisTemplate<String,Object> redisTemplate;


    @PostMapping("/save")
    public boolean save(@RequestBody @Valid SetmealVo setmealVo){

        boolean flag = setmealService.saveSetmeal(setmealVo);
        if (flag){
            //保存成功时，将真正使用的套餐图片从临时垃圾桶中去除，防止被清理。
            redisTemplate.opsForSet().remove("temp-rubbish",setmealVo.getImg());
        }
        return flag;
    }

    @PostMapping("/findByPage")
    public PageResult findByPage(@RequestBody QueryPageBean pageBean){
        return setmealService.findByPage(pageBean);
    }

    @GetMapping("/delete/{id}")
    public boolean delete(@PathVariable("id") String id){
        return setmealService.removeById(id);
    }

    @PostMapping("/upload")
    public String upload(MultipartFile file) throws IOException {
        String key = AliyunFileUtil.upload(file.getInputStream(),file.getOriginalFilename());
        //将key放入redis缓存中，用于清理冗余图片
        redisTemplate.opsForSet().add("temp-rubbish",key);
        //维护辅助key用于记录失效时间,30秒失效
        redisTemplate.opsForValue().set("file:"+key,1,120L, TimeUnit.SECONDS);
        return key;
    }
    @GetMapping("/getSetmealReport")
    public Result<List<Map>> getSetmealReport(){
        List<Map> list = setmealService.getSetmealReport();
        return Result.build(list);
    }

    /**
     * 测试清理图片的方法是否正常。
     * @return
     */
    @GetMapping("/deleteFile")
    public boolean deleteFile(){
        return setmealService.clearOssPic();
    }

}
