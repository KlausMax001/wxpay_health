package com.wang;

import com.wang.core.GenerateCode;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

@SpringBootTest
public class GeneratorCodeTool {

    @Resource
    private GenerateCode generateCode;

    @Test
    public void generator(){
        generateCode.generator();
    }

}
