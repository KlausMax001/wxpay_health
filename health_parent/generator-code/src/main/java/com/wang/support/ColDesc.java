package com.wang.support;

public class ColDesc{
	
	private String tableName;
	
	private String columnName;
	
	private String dataType;
	
	private String comments;
	
	private int iskey;
	
	public ColDesc(){}
	
	public ColDesc(String tableName, String columnName, String dataType, String comments, int isKey){
		this.tableName = tableName;
		this.columnName = columnName;
		this.dataType = dataType;
		this.comments = comments;
		this.iskey = isKey;
	}
	
	public String getTableName() {
		return tableName;
	}
	
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	
	public String getColumnName() {
		return StringUtil.underline2Camel(columnName, true);
	}
	
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}
	
	public String getDataType() {
		return dataType;
	}
	
	public void setDataType(String dataType) {
		this.dataType = dataType;
	}
	
	public String getComments() {
		return comments;
	}
	
	public void setComments(String comments) {
		this.comments = comments;
	}
	
	public int getIskey() {
		return iskey;
	}
	
	public void setIskey(int iskey) {
		this.iskey = iskey;
	}

}
