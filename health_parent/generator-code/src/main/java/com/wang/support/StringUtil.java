package com.wang.support;

import org.springframework.util.StringUtils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.SecureRandom;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtil extends StringUtils {
	
	public static boolean isEmpty(Object obj){
		if(obj == null || "".equals(obj.toString().trim())){
			return true;
		}
		return false;
	}
	
	public static String encodingFileName(String fileName) {
        String returnFileName = "";
        try {
            returnFileName = URLEncoder.encode(fileName, "UTF-8");
            returnFileName = StringUtils.replace(returnFileName, "+", "%20");
            if (returnFileName.length() > 150) {
                returnFileName = new String(fileName.getBytes("GB2312"), "ISO8859-1");
                returnFileName = StringUtils.replace(returnFileName, " ", "%20");
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return returnFileName;
    }
	
	/**
     * 下划线转驼峰法
     * @param line 源字符串
     * @param smallCamel 大小驼峰,是否为小驼峰
     * @return 转换后的字符串
     */
    public static String underline2Camel(String line,boolean smallCamel){
        if(line==null||"".equals(line)){
            return "";
        }
        StringBuffer sb=new StringBuffer();
        Pattern pattern=Pattern.compile("([A-Za-z\\d]+)(_)?");
        Matcher matcher=pattern.matcher(line);
        while(matcher.find()){
            String word=matcher.group();
            sb.append(smallCamel&&matcher.start()==0?Character.toLowerCase(word.charAt(0)):Character.toUpperCase(word.charAt(0)));
            int index=word.lastIndexOf('_');
            if(index>0){
                sb.append(word.substring(1, index).toLowerCase());
            }else{
                sb.append(word.substring(1).toLowerCase());
            }
        }
        return sb.toString();
    }
    /**
     * 驼峰法转下划线
     * @param line 源字符串
     * @return 转换后的字符串
     */
    public static String camel2Underline(String line){
        if(line==null||"".equals(line)){
            return "";
        }
        line=String.valueOf(line.charAt(0)).toUpperCase().concat(line.substring(1));
        StringBuffer sb=new StringBuffer();
        Pattern pattern=Pattern.compile("[A-Z]([a-z\\d]+)?");
        Matcher matcher=pattern.matcher(line);
        while(matcher.find()){
            String word=matcher.group();
            sb.append(word.toUpperCase());
            sb.append(matcher.end()==line.length()?"":"_");
        }
        return sb.toString();
    }
    
    public static boolean isNumeric(String str){
    	for (int i = 0; i < str.length(); i++){
    		if (!Character.isDigit(str.charAt(i))){
    			return false;
    		}
    	}
    	return true;
	}
    
    public static String str2HexStr(String str){
	    char[] chars = "0123456789ABCDEF".toCharArray();
	    StringBuilder sb = new StringBuilder("");
	    byte[] bs = str.getBytes();
	    int bit;
	    for (int i = 0; i < bs.length; i++){
		    bit = (bs[i] & 0x0f0) >> 4;
		    sb.append(chars[bit]);
		    bit = bs[i] & 0x0f;
		    sb.append(chars[bit]);
		    sb.append(' ');
	    }
	    return sb.toString().trim();
    }
    
    public static String generateNick() {
    	SecureRandom random = new SecureRandom();
		StringBuffer nickRandom = new StringBuffer();
		for(int i = 0;i<3;i++) {
			nickRandom.append(random.nextInt(10));
		}
		return "星云" + nickRandom.toString();
    }
    
    public static boolean containsSame(String str1,String str2) {
    	if(isEmpty(str1) == false && isEmpty(str2) == false) {
    		Set<String> result = new HashSet<String>();
    		Set<String> set1 = new HashSet<String>();
    		Set<String> set2 = new HashSet<String>();
    		for(String _str : str1.split(",")) {
    			set1.add(_str);
    		}
    		for(String _str : str2.split(",")) {
    			set2.add(_str);
    		}
    		result.addAll(set1);
    		result.retainAll(set2);
    		if(result.isEmpty() == false) {
    			return true;
    		}
    	}
    	return false;
    }

	public static String convertListToString(List<String> list) {
    	if(list == null || list.isEmpty()) {
    		return null;
    	}
    	StringBuffer sb = new StringBuffer();
    	list.forEach(str ->{
    		sb.append(str).append(",");
    	});
    	sb.deleteCharAt(sb.length() -1);
    	return sb.toString();
    }
    
}
