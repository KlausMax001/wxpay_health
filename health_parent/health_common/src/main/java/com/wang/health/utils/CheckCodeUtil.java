package com.wang.health.utils;

import org.apache.commons.lang3.RandomStringUtils;


public class CheckCodeUtil {
    private static final Integer minnumber = 1000;
    private static final Integer maxNumber = 9999;

    public static String createCheckCode(String phoneNumber) {
        //获取一个四位数的验证码。
        String checkCode = RandomStringUtils.randomNumeric(4);
        //设置验证码到redis，失效时间为5分钟。
        RedisUtil.set("checkCode:"+phoneNumber,checkCode,5);
        return checkCode;
    }

    public static String getCheckCode(String phoneNumber){
        String redisCheckCode = RedisUtil.get("checkCode:" + phoneNumber);
        return redisCheckCode;
    }
}
