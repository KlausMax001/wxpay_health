package com.wang.health.utils;


import org.apache.commons.codec.digest.Md5Crypt;

/**
 * MD5加密工具类
 */
public class MD5Util {

	private static String salt = "$1$2$3";

	public static String encode(String data){
		return Md5Crypt.md5Crypt(data.getBytes(),salt);
	}

	public static void main(String[] args) {
		System.out.println(encode("123"));
	}
}
