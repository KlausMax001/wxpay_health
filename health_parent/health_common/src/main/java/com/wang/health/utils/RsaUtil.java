package com.wang.health.utils;

import java.security.*;

/**
 * RSA加密生成私钥和公钥的工具类
 */
public class RsaUtil {

    private static final String secret = "w1a2n3g4j5u6n7h8u9i1";

    private static KeyPair keyPair;

    static {
        KeyPairGenerator keyPairGenerator = null;
        try {
            keyPairGenerator = KeyPairGenerator.getInstance("RSA");
            SecureRandom secureRandom = new SecureRandom(secret.getBytes());
            keyPairGenerator.initialize(1024, secureRandom);
            keyPair = keyPairGenerator.genKeyPair();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    public static PublicKey getPublicKey(){
        return keyPair.getPublic();
    }
    public static PrivateKey getPrivateKey(){
        return keyPair.getPrivate();
    }

}
