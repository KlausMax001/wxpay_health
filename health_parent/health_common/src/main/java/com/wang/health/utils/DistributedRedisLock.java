package com.wang.health.utils;

import org.springframework.integration.redis.util.RedisLockRegistry;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;

@Component
public class DistributedRedisLock {

    @Resource
    private RedisLockRegistry redisLockRegistry;

    // 加锁
    public Boolean lock(String lockName,long timeout) {
        try {
            if (redisLockRegistry == null) {
                return false;
            }
            Lock lock = redisLockRegistry.obtain(lockName);
            // 锁10秒后自动释放，防止死锁
            return lock.tryLock(timeout, TimeUnit.SECONDS);
        } catch (Exception e) {
            return false;
        }
    }

    // 释放锁
    public Boolean unlock(String lockName) {
        try {
            if (redisLockRegistry == null) {
                return false;
            }
            Lock lock = redisLockRegistry.obtain(lockName);

            lock.unlock();
            // 释放锁成功
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
