package com.wang.health.listener;

import com.wang.health.utils.DistributedRedisLock;
import com.wang.health.utils.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * 监听器，在项目启动时执行。
 */
@Component
public class StartupListener implements ApplicationListener<ContextRefreshedEvent> {
    @Resource
    private RedisTemplate<String,Object> redisTemplate;
    @Resource
    private DistributedRedisLock distributedRedisLock;
    @Autowired(required = false)
    private List<BaseApplicationListener> listeners;
    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        RedisUtil.register(redisTemplate);
        RedisUtil.registerLock(distributedRedisLock);
        if (listeners != null) {
            listeners.stream().forEach(listener -> {
                listener.onApplicationEvent(contextRefreshedEvent);
            });
        }
    }
}
