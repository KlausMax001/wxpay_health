package com.wang.health.utils;

import io.jsonwebtoken.*;

import java.util.Date;

/**
 * 使用Jwt生成token的工具类
 */
public class JwtTokenUtil {

    private static final String DEFAULT_SUBJECT = "WANG_HEALTH_SUBJECT";

    private static final int default_expire_minutes = 60;

    public static String createToken(String tokenId) throws Exception {
        return createToken(tokenId,default_expire_minutes);
    }

    /**
     * 根据字符串创建token
     * @param tokenId
     * @param duration
     * @return
     * @throws Exception
     */
    public static String createToken(String tokenId,int duration) throws Exception {
        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.RS256;
        long _date = System.currentTimeMillis();
        Date now = new Date(_date);
        JwtBuilder builder = Jwts.builder()
                .setId(tokenId)
                .setIssuedAt(now)
                .setSubject(DEFAULT_SUBJECT)
                .signWith(signatureAlgorithm, RsaUtil.getPrivateKey());
        if(duration > 0) {
            _date = _date + duration * 60 * 1000;
            builder.setExpiration(new Date(_date));
        }
        return builder.compact();
    }

    /**
     * 解析token
     * @param token
     * @return
     * @throws Exception
     */
    public static String parseToken(String token) throws Exception{
        Claims claims = null;
        try {
            claims = Jwts.parser()
                    .setSigningKey(RsaUtil.getPublicKey())
                    .parseClaimsJws(token).getBody();
        } catch (ExpiredJwtException e) {
            // TODO: handle exception
            throw new Exception("Token已过期");
        }
        return claims.getId();
    }

    /**
     * 测试jwt生成token
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
      String userName = "wang";
        String token1 = createToken(userName);
        System.out.println(token1);
        System.out.println(parseToken(token1));
    }

}
