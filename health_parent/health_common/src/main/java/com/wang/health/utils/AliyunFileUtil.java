package com.wang.health.utils;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import java.io.InputStream;
import java.util.UUID;

public class AliyunFileUtil {

    // yourEndpoint填写Bucket所在地域对应的Endpoint。以华东1（杭州）为例，Endpoint填写为https://oss-cn-hangzhou.aliyuncs.com。
    private static final String endpoint = "oss-cn-shanghai.aliyuncs.com";
    // 阿里云账号AccessKey拥有所有API的访问权限，风险很高。强烈建议您创建并使用RAM用户进行API访问或日常运维，请登录RAM控制台创建RAM用户。
    private static final String accessKeyId = "LTAI5tEiPtGToE7AzKzS5LNe";
    private static final String accessKeySecret = "h338KFRZNC9OxGLJkdouPfdY8w0Rns";
    private static final String bucketName = "health-wang";

    //public static void main(String[] args) throws FileNotFoundException {
    //
    //    // 创建OSSClient实例。
    //    OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
    //
    //    // 填写本地文件的完整路径。如果未指定本地路径，则默认从示例程序所属项目对应本地路径中上传文件流。
    //    InputStream inputStream = new FileInputStream("D:\\test\\2.jpg");
    //    // 填写Bucket名称和Object完整路径。Object完整路径中不能包含Bucket名称。
    //    ossClient.putObject(bucketName, "2.jpg", inputStream);
    //
    //    // 关闭OSSClient。
    //    ossClient.shutdown();
    //}
    public static String upload(InputStream inputStream,String fileName){
        // 创建OSSClient实例。
        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);

        // 填写Bucket名称和Object完整路径。Object完整路径中不能包含Bucket名称。
        String fileKey = UUID.randomUUID().toString()+fileName.substring(fileName.lastIndexOf("."));
        ossClient.putObject(bucketName, "img/"+fileKey, inputStream);

        // 关闭OSSClient。
        ossClient.shutdown();
        return fileKey;
    }
    public static boolean deleteFile(String fileName){
        // 创建OSSClient实例。
        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);

        // 填写Bucket名称和Object完整路径。Object完整路径中不能包含Bucket名称。
        ossClient.deleteObject(bucketName,"img/"+fileName);

        // 关闭OSSClient。
        ossClient.shutdown();
        return true;
    }
}
