package com.wang.health.listener;

import org.springframework.context.event.ContextRefreshedEvent;

public  interface BaseApplicationListener {
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent);
}
