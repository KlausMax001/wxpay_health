package com.wang.health.utils;

import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.exceptions.ServerException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.CommonRequest;

/*
pom.xml
<dependency>
  <groupId>com.aliyun</groupId>
  <artifactId>aliyun-java-sdk-core</artifactId>
  <version>4.5.3</version>
</dependency>
*/


public class SMSUtil {

    public static final String VALIDATE_CODE = "SMS_55665182";//发送短信验证码
    public static final String ORDER_NOTICE = "SMS_55665182";//体检预约成功通知
    public static final String ACCESS_KEY_ID = "LTAIA2KE6onERO5f";
    public static final String ACCESS_SECRET="raxTWs3twt1k9ESJrS2kUyrDUMFoGx";

    //public static void main(String[] args) {
    //    DefaultProfile profile = DefaultProfile.getProfile("cn-hangzhou", ACCESS_KEY_ID ,ACCESS_SECRET);
    //    IAcsClient client = new DefaultAcsClient(profile);
    //
    //    CommonRequest request = new CommonRequest();
    //    request.setSysMethod(MethodType.POST);
    //    request.setSysDomain("dysmsapi.aliyuncs.com");
    //    request.setSysVersion("2017-05-25");
    //    request.setSysAction("SendSms");
    //    request.putQueryParameter("RegionId", "cn-hangzhou");
    //    request.putQueryParameter("PhoneNumbers", "15738713395");
    //    request.putQueryParameter("SignName", "健康管理");
    //    request.putQueryParameter("TemplateCode", VALIDATE_CODE);
    //    request.putQueryParameter("TemplateParam", "{\"code\":\"9527\"}");
    //    try {
    //        CommonResponse response = client.getCommonResponse(request);
    //        System.out.println(response.getData());
    //    } catch (ServerException e) {
    //        e.printStackTrace();
    //    } catch (ClientException e) {
    //        e.printStackTrace();
    //    }
    //}

    public static void sendMsg(String phoneNumber,String code,String type){
        DefaultProfile profile = DefaultProfile.getProfile("cn-hangzhou", ACCESS_KEY_ID ,ACCESS_SECRET);
        IAcsClient client = new DefaultAcsClient(profile);

        CommonRequest request = new CommonRequest();
        request.setSysMethod(MethodType.POST);
        request.setSysDomain("dysmsapi.aliyuncs.com");
        request.setSysVersion("2017-05-25");
        request.setSysAction("SendSms");
        request.putQueryParameter("RegionId", "cn-hangzhou");
        request.putQueryParameter("PhoneNumbers", phoneNumber);
        request.putQueryParameter("SignName", "健康管理");

        if ("register".equals(type)){
            request.putQueryParameter("TemplateCode", VALIDATE_CODE);
        }else if ("success".equals(type)){
            request.putQueryParameter("TemplateCode", ORDER_NOTICE);
        }
        request.putQueryParameter("TemplateParam", "{\"code\":\""+code+"\"}");
        try {
            CommonResponse response = client.getCommonResponse(request);
            System.out.println(response.getData());
        } catch (ServerException e) {
            e.printStackTrace();
        } catch (ClientException e) {
            e.printStackTrace();
        }
    }


}
