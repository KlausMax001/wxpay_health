package com.wang.health.utils;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class POIUtil {
    private static final String XLS = "xls";
    private static final String XLSX = "xlsx";

    public static List<String[]> readExcel(InputStream inputStream,String fileName) throws Exception {
        return readExcel(inputStream,fileName,1);
    }
    public static List<String[]> readExcel(InputStream inputStream,String fileName,Integer startRow) throws Exception {
        //检验是否是一个Excel文件
        checkFile(fileName);
        //获得Workbook工作薄对象
        Workbook workbook = getWorkBook(inputStream,fileName);
        //创建返回对象，把每行中的值作为一个数组，所有行作为一个集合返回
        List<String[]> list = new ArrayList<>();
        if (workbook!=null){
            //遍历所有的工作表
            for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
                //获得当前sheet工作表
                Sheet sheet = workbook.getSheetAt(i);
                if (sheet==null){
                    //跳过当前工作表循环，进入下一个
                    continue;
                }
                //获取第一行的索引
                int firstRowNum = sheet.getFirstRowNum();
                //获取最后一行的索引。
                int lastRowNum = sheet.getLastRowNum();
                //遍历所有行
                for (int j = firstRowNum+startRow; j <=lastRowNum; j++) {
                    Row row = sheet.getRow(j);
                    if (row==null){
                        //如果行为null，跳出本行循环。
                        continue;
                    }
                    short firstCellNum = row.getFirstCellNum();
                    short lastCellNum = row.getLastCellNum();
                    //创建数组存储每行的数据
                    String[] values = new String[lastCellNum];
                    //遍历所有单元格
                    for (int k = firstCellNum; k < lastCellNum; k++) {
                        Cell cell = row.getCell(k);
                        values[k] = getCellValue(cell);
                    }
                    list.add(values);
                }
            }
            workbook.close();
        }
        return list;
    }

    private static String getCellValue(Cell cell) {
        String cellValue = "";
        if(cell == null){
            return cellValue;
        }
        //如果当前单元格内容为日期类型，需要特殊处理
        String dataFormatString = cell.getCellStyle().getDataFormatString();
        if(dataFormatString.equals("m/d/yy")){
            cellValue = DateUtil.formatDate(cell.getDateCellValue());
            return cellValue;
        }
        //把数字当成String来读，避免出现1读成1.0的情况
        if(cell.getCellType() == Cell.CELL_TYPE_NUMERIC){
            cell.setCellType(Cell.CELL_TYPE_STRING);
        }
        //判断数据的类型
        switch (cell.getCellType()){
            case Cell.CELL_TYPE_STRING: //字符串
                cellValue = String.valueOf(cell.getStringCellValue());
                break;
            case Cell.CELL_TYPE_BOOLEAN: //Boolean
                cellValue = String.valueOf(cell.getBooleanCellValue());
                break;
            case Cell.CELL_TYPE_FORMULA: //公式
                cellValue = String.valueOf(cell.getCellFormula());
                break;
            case Cell.CELL_TYPE_BLANK: //空值
                cellValue = "";
                break;
            case Cell.CELL_TYPE_ERROR: //故障
                cellValue = "非法字符";
                break;
            default:
                cellValue = "未知类型";
                break;
        }
        return cellValue;
    }

    private static Workbook getWorkBook(InputStream inputStream, String fileName) {
        try {
            if (fileName.endsWith(XLSX)){
                //2007版之后
                return new XSSFWorkbook(inputStream);
            }else if (fileName.endsWith(XLS)){
                //2003-2007版之后
                return new HSSFWorkbook(inputStream);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private static void checkFile(String fileName) throws Exception {
        if (fileName==null){
            throw new FileNotFoundException("文件不存在");
        }
        if (!(fileName.endsWith(XLS)||fileName.endsWith(XLSX))){

            throw new IOException(fileName+":不是Excel文件");
        }
    }
}
