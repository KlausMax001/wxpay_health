package com.wang.health.web;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.wang.health.pojo.OrderInfo;
import com.wang.health.service.OrderInfoService;
import com.wang.health.vo.Result;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @Description
 * @Author Guo Lixin
 * @date 2021/3/27 11:30
 */

@RestController
@RequestMapping("/orderPay")
public class OrderPayController {

    @DubboReference
    private OrderInfoService orderInfoService;

    /**
     * 支付通知
     * @param request
     * @param response
     * @return
     */
    @PostMapping("payNotify")
    public void payNotify(HttpServletRequest request, HttpServletResponse response) throws IOException {

        // 1.接收请求参数（xml）
        ServletInputStream in = request.getInputStream();
        // 2.将xml转为java对象
        XmlMapper xmlMapper = new XmlMapper();
        Map param = xmlMapper.readValue(in, Map.class);

        // 3.调用orderService，修改订单状态
       orderInfoService.updatePayStatus(param);

        // 4.返回微信平台，接收成功..
        HashMap<String, String> result = new HashMap<>();
        result.put("return_code", "SUCCESS");
        result.put("return_msg", "OK");

        // 将map转为xml
        String xml = xmlMapper.writeValueAsString(result);
        response.setContentType("application/xml;charset=utf-8");
        response.getWriter().write(xml);

    }

    /**
     * 查询订单支付状态是否为已支付
     * @param orderId
     * @return
     */
    @GetMapping("queryPayStatus/{orderId}")
    public Result<Boolean> queryPayStatus(@PathVariable("orderId") String orderId){
        OrderInfo orderInfo = orderInfoService.findOrderInfoByOrderId(orderId);
        if (orderInfo.getPayStatus() == 1) {
            return Result.build(true);
        }else {
            return Result.build(false);
        }
    }

}
