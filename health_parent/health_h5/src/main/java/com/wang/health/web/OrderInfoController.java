package com.wang.health.web;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.wang.health.dto.OrderInfoDto;
import com.wang.health.service.OrderInfoService;
import com.wang.health.utils.PayUtils;
import com.wang.health.vo.Result;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/orderInfo")
public class OrderInfoController {

    @DubboReference
    private OrderInfoService orderInfoService;

    @Autowired
    private AmqpTemplate amqpTemplate;



    @GetMapping("/sendCheckCode/{phoneNumber}")
    public Result sendCheckCode(@PathVariable("phoneNumber") String phoneNumber){
        amqpTemplate.convertAndSend("sms_exchange","sms",phoneNumber);
        //发送验证码,使用mq消息队列
        //orderInfoService.sendCheckCode(phoneNumber,"register");
        return Result.build(true);
    }
    @PostMapping("/addOrderInfo")
    public Result addOrderInfo(@RequestBody Map map){
        String orderId= orderInfoService.addOrderInfo(map);
        return Result.build(orderId);
    }
    @GetMapping("findOrderInfoById/{id}")
    public Result findOrderInfoById(@PathVariable("id") String id){
        OrderInfoDto orderInfoDto = orderInfoService.findOrderInfoById(id);
        return Result.build(orderInfoDto);
    }

    //生成微信订单
    @PostMapping("createOrder")
    public Result<String> createOrder(@RequestBody OrderInfoDto orderInfoDto){

        String orderId = orderInfoDto.getOrderNo();

        //设置订单流水号和支付金额
        String order = PayUtils.createOrder(orderId,1);

        return Result.build(order);
    }

}
