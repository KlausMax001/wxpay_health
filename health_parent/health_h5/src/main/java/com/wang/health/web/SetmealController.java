package com.wang.health.web;

import com.wang.health.dto.SetmealDto;
import com.wang.health.pojo.Setmeal;
import com.wang.health.service.SetmealService;
import com.wang.health.vo.PageResult;
import com.wang.health.vo.QueryPageBean;
import com.wang.health.vo.Result;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/setmeal")
public class SetmealController {

    @DubboReference
    private SetmealService setmealService;

    @GetMapping("/findAll")
    public Result<List<Setmeal>> findAll(){

        return Result.build(setmealService.list());
    }
    @PostMapping("/findByPage")
    public Result findByPage(@RequestBody QueryPageBean pageBean){
        PageResult pageResult = setmealService.findByPage(pageBean);
        return Result.build(pageResult);
    }

    @GetMapping("/findSetmealDtoById/{setmealId}")
    public Result<SetmealDto> findSetmealDtoById(@PathVariable("setmealId") Long setmealId){
        return Result.build(setmealService.findSetmealDtoById(setmealId));
    }
    @GetMapping("/findSetmealById/{id}")
    public Result<Setmeal> findSetmealById(@PathVariable("id") String id){
        return Result.build(setmealService.getById(id));
    }
}
