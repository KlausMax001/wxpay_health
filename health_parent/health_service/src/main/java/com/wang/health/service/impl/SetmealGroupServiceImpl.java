package com.wang.health.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.wang.health.service.SetmealGroupService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wang.health.dao.SetmealGroupMapper;
import com.wang.health.pojo.SetmealGroup;
import org.apache.dubbo.config.annotation.DubboService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@DubboService
public class SetmealGroupServiceImpl extends ServiceImpl<SetmealGroupMapper, SetmealGroup> implements SetmealGroupService {

    @Override
    public void deleteBySetmealId(Long id) {
        QueryWrapper<SetmealGroup> setmealGroupQueryWrapper = new QueryWrapper<>();
        setmealGroupQueryWrapper.lambda().eq(SetmealGroup::getSetmealId,id);
        remove(setmealGroupQueryWrapper);
    }

    @Override
    public void addSetmealGroups(Long id, Long[] groupIds) {
        List<SetmealGroup> list = new ArrayList<>();
        Arrays.stream(groupIds).forEach((groupId)->{
            SetmealGroup setmealGroup = new SetmealGroup();
            setmealGroup.setGroupId(groupId);
            setmealGroup.setSetmealId(id);
            list.add(setmealGroup);
        });
        saveBatch(list);
    }

    @Override
    public List<Long> findBySetmealId(String id) {
        return baseMapper.findBySetmealId(id);
    }
}