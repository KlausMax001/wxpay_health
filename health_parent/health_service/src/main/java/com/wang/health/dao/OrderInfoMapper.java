package com.wang.health.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wang.health.pojo.OrderInfo;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

public interface OrderInfoMapper extends BaseMapper<OrderInfo> {
    //更新订单支付状态
    @Update("")
    Boolean updateState(@Param("oid") String oid);


}
