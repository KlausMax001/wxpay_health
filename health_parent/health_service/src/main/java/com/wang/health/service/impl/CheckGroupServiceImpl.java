package com.wang.health.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.Query;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wang.health.dao.CheckGroupMapper;
import com.wang.health.dto.CheckGroupDto;
import com.wang.health.dto.SetmealDto;
import com.wang.health.pojo.CheckGroup;
import com.wang.health.pojo.CheckItem;
import com.wang.health.pojo.Setmeal;
import com.wang.health.service.CheckGroupItemService;
import com.wang.health.service.CheckGroupService;
import com.wang.health.service.CheckItemService;
import com.wang.health.utils.BeanConv;
import com.wang.health.vo.CheckGroupVo;
import com.wang.health.vo.PageResult;
import com.wang.health.vo.QueryPageBean;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;


@DubboService
public class CheckGroupServiceImpl extends ServiceImpl<CheckGroupMapper, CheckGroup> implements CheckGroupService {

    @Autowired
    private CheckGroupItemService checkGroupItemService;
    @Resource
    private CheckItemService checkItemService;

    @Override
    @Transactional//事务控制
    public boolean saveGroup(CheckGroupVo checkGroupVo) {

        //添加或修改检查组
        this.saveOrUpdate(checkGroupVo);

        //删除所有旧关系
        checkGroupItemService.deleteByGroupId(checkGroupVo.getId());

        //添加新关系
        checkGroupItemService.addGroupItemsByItemIds(checkGroupVo.getId(),checkGroupVo.getItemIds());
        return true;
    }

    @Override
    public CheckGroupDto findCheckGroupDtoDtoById(Long checkGroupId) {
        //根据id找CheckGroup
        CheckGroup checkGroup = getById(checkGroupId);
        //转换对象
        CheckGroupDto checkGroupDto = BeanConv.toBean(checkGroup, CheckGroupDto.class);
        //查找CheckItem集合
        List<CheckItem> checkItemList = checkItemService.findCheckItemsByCheckGroupId(checkGroupId);
        checkGroupDto.setCheckItems(checkItemList);
        return checkGroupDto;
    }

    @Override
    public PageResult findByPage(QueryPageBean pageBean) {
        QueryWrapper<CheckGroup> checkGroupQueryWrapper = new QueryWrapper<>();
        if (pageBean.getQueryString()!=null&&pageBean.getQueryString().length()>0) {
            checkGroupQueryWrapper.like("code", pageBean.getQueryString()).or()
                    .like("name", pageBean.getQueryString()).or()
                    .like("helpCode", pageBean.getQueryString());
        }
        //Page<CheckGroup> page = baseMapper.MyPage(new Page<>(pageBean.getCurrentPage(), pageBean.getPageSize()),checkGroupQueryWrapper);
        Page<CheckGroup> page = page(new Page<>(pageBean.getCurrentPage(), pageBean.getPageSize()),checkGroupQueryWrapper);
        return new PageResult(page.getTotal(),page.getRecords());
    }
}
