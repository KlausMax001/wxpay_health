package com.wang.health;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.FileNotFoundException;;

@SpringBootApplication
@MapperScan("com.wang.health.dao")
@EnableDubbo(scanBasePackages = "com.wang.health.service.impl")
public class ServiceApplication {
    public static void main(String[] args) throws FileNotFoundException {
        SpringApplication.run(ServiceApplication.class,args);
    }
}
