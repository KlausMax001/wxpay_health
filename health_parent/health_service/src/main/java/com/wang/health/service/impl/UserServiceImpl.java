package com.wang.health.service.impl;

import com.wang.health.Do.UserDo;
import com.wang.health.pojo.Role;
import com.wang.health.service.RoleUserService;
import com.wang.health.service.UserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wang.health.dao.UserMapper;
import com.wang.health.pojo.User;
import com.wang.health.utils.BeanConv;
import org.apache.dubbo.config.annotation.DubboService;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@DubboService
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    @Resource
    private RoleUserService roleUserService;

    @Override
    public List<UserDo> userDoList() {

        List<User> list = this.list();
        List<UserDo> userDoList =BeanConv.toBeanList(list,UserDo.class);
        userDoList.stream().forEach(userDo -> {
            List<Role> roleList = roleUserService.findRoleByUserId(userDo.getId()+"");
            userDo.setRoleList(roleList);
        });
        return userDoList;
    }
}