package com.wang.health.service.impl;

import com.wang.health.service.PermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wang.health.dao.PermissionMapper;
import com.wang.health.pojo.Permission;
import org.apache.dubbo.config.annotation.DubboService;

@DubboService
public class PermissionServiceImpl extends ServiceImpl<PermissionMapper, Permission> implements PermissionService {

}