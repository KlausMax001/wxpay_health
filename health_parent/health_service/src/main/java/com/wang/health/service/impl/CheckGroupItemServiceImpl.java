package com.wang.health.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wang.health.dao.CheckGroupItemMapper;
import com.wang.health.pojo.CheckGroupItem;
import com.wang.health.service.CheckGroupItemService;
import org.apache.dubbo.config.annotation.DubboService;

import java.util.ArrayList;
import java.util.List;

@DubboService
public class CheckGroupItemServiceImpl extends ServiceImpl<CheckGroupItemMapper, CheckGroupItem> implements CheckGroupItemService {
    @Override
    public void deleteByGroupId(Long id) {
        QueryWrapper<CheckGroupItem> checkGroupItemQueryWrapper = new QueryWrapper<>();
        checkGroupItemQueryWrapper.lambda().eq(CheckGroupItem::getGroupId,id);
        this.remove(checkGroupItemQueryWrapper);
    }

    @Override
    public void addGroupItemsByItemIds(Long id, Long[] itemIds) {
        List<CheckGroupItem> list = new ArrayList();
        for (Long itemId : itemIds) {
            CheckGroupItem checkGroupItem = new CheckGroupItem();
            checkGroupItem.setGroupId(id);
            checkGroupItem.setItemId(itemId);
            list.add(checkGroupItem);
        }
        this.saveBatch(list);
    }

    @Override
    public List<Long> findByGroupId(String groupId) {
        return baseMapper.findByGroupId(groupId);
    }
}
