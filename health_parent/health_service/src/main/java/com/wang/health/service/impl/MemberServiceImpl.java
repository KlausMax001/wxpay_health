package com.wang.health.service.impl;

import com.wang.health.service.MemberService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wang.health.dao.MemberMapper;
import com.wang.health.pojo.Member;
import com.wang.health.utils.DateUtil;
import org.apache.dubbo.config.annotation.DubboService;

import java.util.*;

@DubboService
public class MemberServiceImpl extends ServiceImpl<MemberMapper, Member> implements MemberService {

    @Override
    public Map findMemberByDate(List<Date> dateList) {
        HashMap<String, List<Object>> map = new HashMap<>();
        //判断间隔，月份大于12个月，按季度，年份大于三年，按年份。
        Date startDate = dateList.get(0);
        Date endDate = dateList.get(1);
        int month = endDate.getMonth() - startDate.getMonth();
        int year = endDate.getYear() - startDate.getYear();
        List<Map> list = null;
        if ((year>1&&year<=3)||(year==1&&month>=0)){
            //按季度查询
        }else if (year>3){
            //按年查询
        }else{
            //按月查询
            Long start = DateUtil.getDateId(startDate)!=null ? Long.parseLong(DateUtil.getDateId(startDate)) : null;
            Long end = DateUtil.getDateId(endDate)!=null ? Long.parseLong(DateUtil.getDateId(endDate)) : null;
            list = baseMapper.findByMonth(start,end);
        }
        if (list!=null){
            ArrayList<Object> date = new ArrayList<>();

            ArrayList<Object> value = new ArrayList<>();
            for (Map map1 : list) {
                date.add(map1.get("fyear")+"-"+map1.get("fmonth"));
                value.add(map1.get("value"));
            }
            map.put("date",date);
            map.put("value",value);
            //date:["2020-6", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"]

            //value:[820, 932, 901, 934, 1290, 1330, 1320]
        }
        return map;
    }

}