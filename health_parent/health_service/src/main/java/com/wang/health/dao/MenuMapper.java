package com.wang.health.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wang.health.pojo.Menu;

public interface MenuMapper extends BaseMapper<Menu> {
}
