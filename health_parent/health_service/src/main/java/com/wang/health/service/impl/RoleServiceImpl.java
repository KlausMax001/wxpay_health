package com.wang.health.service.impl;

import com.wang.health.service.RoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wang.health.dao.RoleMapper;
import com.wang.health.pojo.Role;
import org.apache.dubbo.config.annotation.DubboService;

@DubboService
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements RoleService {

}