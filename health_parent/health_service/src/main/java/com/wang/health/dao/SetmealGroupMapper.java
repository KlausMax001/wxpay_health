package com.wang.health.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wang.health.pojo.SetmealGroup;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface SetmealGroupMapper extends BaseMapper<SetmealGroup> {
    @Select("select group_id from tb_setmeal_group where setmeal_id = #{id}")
    List<Long> findBySetmealId(@Param("id") String id);
}
