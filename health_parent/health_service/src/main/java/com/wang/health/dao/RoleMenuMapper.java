package com.wang.health.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wang.health.pojo.RoleMenu;

public interface RoleMenuMapper extends BaseMapper<RoleMenu> {
}
