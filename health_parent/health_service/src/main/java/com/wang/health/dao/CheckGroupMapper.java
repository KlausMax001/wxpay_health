package com.wang.health.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wang.health.pojo.CheckGroup;
import org.apache.ibatis.annotations.Select;

public interface CheckGroupMapper extends BaseMapper<CheckGroup> {
    @Select("select * from tb_group where delete_flag=0")
    Page<CheckGroup> MyPage(Page<Object> objectPage);
}
