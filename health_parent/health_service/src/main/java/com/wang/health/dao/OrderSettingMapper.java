package com.wang.health.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wang.health.pojo.OrderSetting;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

public interface OrderSettingMapper extends BaseMapper<OrderSetting> {

    @Update("DELETE FROM tb_ordersetting WHERE  DATE(order_date) <= DATE(DATE_SUB(NOW(),INTERVAL 1 YEAR));")
    boolean clearOrderDate();
}
