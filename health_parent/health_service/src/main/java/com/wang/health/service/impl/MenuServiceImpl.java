package com.wang.health.service.impl;

import com.wang.health.service.MenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wang.health.dao.MenuMapper;
import com.wang.health.pojo.Menu;
import org.apache.dubbo.config.annotation.DubboService;

@DubboService
public class MenuServiceImpl extends ServiceImpl<MenuMapper, Menu> implements MenuService {

}