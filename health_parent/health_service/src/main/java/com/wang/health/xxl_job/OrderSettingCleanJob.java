package com.wang.health.xxl_job;

import com.wang.health.service.OrderSettingService;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @Description
 * @Author Guo Lixin
 * @date 2021/3/26 15:15
 */
@JobHandler(value = "orderSetting.clean.orderDate.job") //  web配置JobHandler的名称
@Component
public class OrderSettingCleanJob extends IJobHandler {
    @Resource
    private OrderSettingService orderSettingService;


    public ReturnT<String> execute(String param) throws Exception {
        System.out.println("清理冗余数据成功!!!!!!!!!");
        orderSettingService.clearOrderDate();
        return IJobHandler.SUCCESS;
    }
}
