package com.wang.health.listener;


import com.wang.health.Do.UserDo;
import com.wang.health.cst.SysConsts;
import com.wang.health.service.UserService;
import com.wang.health.utils.RedisUtil;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;


import javax.annotation.Resource;
import java.util.List;
@Component
public class MyStartupListener implements BaseApplicationListener {

    @Resource
    private UserService userService;
    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        List<UserDo> userDoList = userService.userDoList();
        userDoList.stream().forEach(userDo -> {
            RedisUtil.set(SysConsts.REDIS_USER_PREFIX+userDo.getUserName(),userDo);
        });
    }
}
