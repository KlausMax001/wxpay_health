package com.wang.health.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wang.health.pojo.Setmeal;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

public interface SetmealMapper extends BaseMapper<Setmeal> {
    @Select("SELECT COUNT(o.`setmeal_id`) value,s.`name` FROM tb_order o,tb_setmeal s WHERE o.`setmeal_id` = s.`id` GROUP BY o.`setmeal_id`")
    List<Map> getSetmealReport();
}
