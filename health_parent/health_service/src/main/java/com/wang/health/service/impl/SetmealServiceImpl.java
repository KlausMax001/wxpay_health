package com.wang.health.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wang.health.dto.CheckGroupDto;
import com.wang.health.dto.SetmealDto;
import com.wang.health.pojo.CheckGroup;
import com.wang.health.service.CheckGroupService;
import com.wang.health.service.SetmealGroupService;
import com.wang.health.service.SetmealService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wang.health.dao.SetmealMapper;
import com.wang.health.pojo.Setmeal;
import com.wang.health.utils.AliyunFileUtil;
import com.wang.health.utils.BeanConv;
import com.wang.health.vo.PageResult;
import com.wang.health.vo.QueryPageBean;
import com.wang.health.vo.SetmealVo;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.BeanUtils;
import org.springframework.data.redis.core.RedisTemplate;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

@DubboService
public class SetmealServiceImpl extends ServiceImpl<SetmealMapper, Setmeal> implements SetmealService {

    @Resource
    private SetmealGroupService setmealGroupService;

    @Resource
    private RedisTemplate<String,Object> redisTemplate;

    @Resource
    private CheckGroupService checkGroupService;

    @Override
    public boolean saveSetmeal(SetmealVo setmealVo) {
        this.saveOrUpdate(setmealVo);

        setmealGroupService.deleteBySetmealId(setmealVo.getId());

        setmealGroupService.addSetmealGroups(setmealVo.getId(),setmealVo.getGroupIds());

        return true;
    }

    @Override
    public PageResult findByPage(QueryPageBean pageBean) {
        QueryWrapper<Setmeal> setmealQueryWrapper = new QueryWrapper<>();

        if (Objects.nonNull(pageBean.getQueryString())&&pageBean.getQueryString().length()>0){
            setmealQueryWrapper.lambda().like(Setmeal::getCode,pageBean.getQueryString()).or()
                    .like(Setmeal::getName,pageBean.getQueryString()).or()
                    .like(Setmeal::getHelpCode,pageBean.getQueryString());
        }

        Page<Setmeal> page = page(new Page<>(pageBean.getCurrentPage(), pageBean.getPageSize()), setmealQueryWrapper);

        return new PageResult(page.getTotal(),page.getRecords());
    }

    /**
     * 清理冗余图片
     * @return
     */
    public boolean clearOssPic(){
        redisTemplate.opsForValue();
        //从缓存中取出临时垃圾桶里的所有图片路径
        Set<String> picsSet = redisTemplate.opsForSet().members("temp-rubbish")
                //转换为stream流
                .stream()
                //将object类型转换为String类型
                .map(file->(String)file)
                //再次收集为set集合
                .collect(Collectors.toSet());
        picsSet.stream().forEach(pic->{
            //拼接辅助key，判断是否过期。
            if (redisTemplate.hasKey("file:"+pic)==false){
                //如果辅助key过期，说明该图片需要被清除
                AliyunFileUtil.deleteFile(pic);
                //从临时垃圾桶中去除
                redisTemplate.opsForSet().remove("temp-rubbish",pic);
            }
        });
        return true;
    }

    @Override
    public SetmealDto findSetmealDtoById(Long setmealId) {
        //根据id找Setmeal
        Setmeal setmeal = getById(setmealId);
        //转换对象
        SetmealDto setmealDto = BeanConv.toBean(setmeal, SetmealDto.class);
        //查找CheckGroupDto集合
        List<Long> checkGroupIds = setmealGroupService.findBySetmealId(setmealId+"");
        List<CheckGroupDto> checkGroupDtoList = new ArrayList<>();
        checkGroupIds.stream().forEach(checkGroupId->{
            checkGroupDtoList.add(checkGroupService.findCheckGroupDtoDtoById(checkGroupId));
        });
        setmealDto.setCheckGroups(checkGroupDtoList);
        return setmealDto;
    }

    @Override
    public List<Map> getSetmealReport() {
        return baseMapper.getSetmealReport();
    }
}