package com.wang.health.service.impl;

import com.wang.health.service.RoleMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wang.health.dao.RoleMenuMapper;
import com.wang.health.pojo.RoleMenu;
import org.apache.dubbo.config.annotation.DubboService;

@DubboService
public class RoleMenuServiceImpl extends ServiceImpl<RoleMenuMapper, RoleMenu> implements RoleMenuService {

}