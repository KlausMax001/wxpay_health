package com.wang.health.mq;

import com.wang.health.utils.CheckCodeUtil;
import com.wang.health.utils.RedisUtil;
import com.wang.health.utils.SMSUtil;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class SmsConsumer implements InitializingBean {

    @Resource
    private RedisTemplate<String,Object> redisTemplate;

    @RabbitListener(queues = "sms_queue")
    public void recv(String phoneNumber){
        String checkCode = CheckCodeUtil.createCheckCode(phoneNumber)+"";
        //资源有限，打印到控制台，模拟发送给用户。
        System.out.println(checkCode);
        //使用该方法可以实现真正的发送给用户验证信息。
        //SMSUtil.sendMsg(phoneNumber,checkCode,"register");
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        RedisUtil.register(redisTemplate);
    }
}
