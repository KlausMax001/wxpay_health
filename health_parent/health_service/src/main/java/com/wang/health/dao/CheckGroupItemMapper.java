package com.wang.health.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wang.health.pojo.CheckGroupItem;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface CheckGroupItemMapper extends BaseMapper<CheckGroupItem> {
    @Select("select item_id from tb_group_item where group_id = #{groupId}")
    List<Long> findByGroupId(@Param("groupId") String groupId);
}
