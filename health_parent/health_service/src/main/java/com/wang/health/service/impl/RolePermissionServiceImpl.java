package com.wang.health.service.impl;

import com.wang.health.service.RolePermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wang.health.dao.RolePermissionMapper;
import com.wang.health.pojo.RolePermission;
import org.apache.dubbo.config.annotation.DubboService;

@DubboService
public class RolePermissionServiceImpl extends ServiceImpl<RolePermissionMapper, RolePermission> implements RolePermissionService {

}