package com.wang.health.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.wang.health.service.OrderSettingService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wang.health.dao.OrderSettingMapper;
import com.wang.health.pojo.OrderSetting;
import com.wang.health.utils.DateUtil;
import com.wang.health.utils.RedisUtil;
import org.apache.dubbo.config.annotation.DubboService;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@DubboService(timeout = 5000)
public class OrderSettingServiceImpl extends ServiceImpl<OrderSettingMapper, OrderSetting> implements OrderSettingService {


    @Override
    public void importData(List<String[]> list) {
        //为防止超时重传导致的数据重复，在此加分布式锁。
        try {
            //设置抢锁超时时间为3秒
            boolean lock = RedisUtil.lock("lock:importData",3);
            //抢锁成功返回true
            if (lock) {
                //抢锁成功,执行业务
                if (list != null && list.isEmpty() != true) {
                    List<OrderSetting> orderSettingList = new ArrayList<>();
                    list.stream().forEach(cell -> {
                        OrderSetting orderSetting = findByDate(cell[0]);
                        if (orderSetting == null) {
                            orderSetting = new OrderSetting();
                        }
                        orderSetting.setNumber(Integer.parseInt(cell[1]));
                        orderSetting.setOrderDate(DateUtil.parseDate(cell[0]));
                        orderSettingList.add(orderSetting);
                    });
                    saveOrUpdateBatch(orderSettingList);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //释放锁
            RedisUtil.unlock("lock:importData");
        }
    }

    @Override
    public Map findByYearMonth(String yearMonth) {
        QueryWrapper<OrderSetting> orderSettingQueryWrapper = new QueryWrapper<>();
        orderSettingQueryWrapper.lambda().likeRight(OrderSetting::getOrderDate, yearMonth);
        List<OrderSetting> list = list(orderSettingQueryWrapper);
        Map<String, OrderSetting> stringOrderSettingMap = new HashMap<>();
        list.stream().forEach(orderSetting -> {
            stringOrderSettingMap.put(DateUtil.formatDate(orderSetting.getOrderDate()), orderSetting);
        });
        return stringOrderSettingMap;
    }

    public OrderSetting findByDate(String date) {
        QueryWrapper<OrderSetting> orderSettingQueryWrapper = new QueryWrapper<>();
        orderSettingQueryWrapper.lambda().likeRight(OrderSetting::getOrderDate, date);
        return getOne(orderSettingQueryWrapper);
    }

    public Boolean updateByDate(String date, Integer number) {
        OrderSetting orderSetting = findByDate(date);
        if (orderSetting == null) {
            orderSetting = new OrderSetting();
        }
        orderSetting.setOrderDate(DateUtil.parseDate(date));
        orderSetting.setNumber(number);
        return saveOrUpdate(orderSetting);
    }

    /**
     * 清理预约日期垃圾记录
     * 清理周期季度  4个季度前 1个年之前
     * @return
     */
    @Override
    public boolean clearOrderDate() {
        return baseMapper.clearOrderDate();
    }
}