package com.wang.health.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wang.health.dao.CheckItemMapper;
import com.wang.health.pojo.CheckItem;
import com.wang.health.service.CheckItemService;
import com.wang.health.vo.PageResult;
import com.wang.health.vo.QueryPageBean;
import org.apache.dubbo.config.annotation.DubboService;
import org.apache.dubbo.config.annotation.Service;

import java.util.List;

@Service
public class CheckItemServiceImpl extends ServiceImpl<CheckItemMapper, CheckItem> implements CheckItemService {

    @Override
    public PageResult findByPage(QueryPageBean pageBean) {

        QueryWrapper<CheckItem> wrapper = new QueryWrapper();
        if (pageBean.getQueryString()!=null&&pageBean.getQueryString().length()>0) {
            wrapper.like("code", pageBean.getQueryString()).or().like("name", pageBean.getQueryString());
        }
        Page<CheckItem> page = this.page(new Page<>(pageBean.getCurrentPage(), pageBean.getPageSize()),wrapper);
        return new PageResult<CheckItem>(page.getTotal(),page.getRecords());
    }

    @Override
    public List<CheckItem> findCheckItemsByCheckGroupId(Long checkGroupId) {
        return baseMapper.findCheckItemsByCheckGroupId(checkGroupId);
    }
}
