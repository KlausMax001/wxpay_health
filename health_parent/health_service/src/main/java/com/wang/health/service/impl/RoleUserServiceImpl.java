package com.wang.health.service.impl;

import com.wang.health.pojo.Role;
import com.wang.health.service.RoleUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wang.health.dao.RoleUserMapper;
import com.wang.health.pojo.RoleUser;
import org.apache.dubbo.config.annotation.DubboService;

import java.util.List;

@DubboService
public class RoleUserServiceImpl extends ServiceImpl<RoleUserMapper, RoleUser> implements RoleUserService {

    @Override
    public List<Role> findRoleByUserId(String userId) {
        return baseMapper.findRoleByUserId(userId);
    }
}