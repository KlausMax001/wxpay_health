package com.wang.health.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wang.health.pojo.RolePermission;

public interface RolePermissionMapper extends BaseMapper<RolePermission> {
}
