package com.wang.health.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.api.R;
import com.wang.health.dto.OrderInfoDto;
import com.wang.health.pojo.Member;
import com.wang.health.pojo.OrderSetting;
import com.wang.health.pojo.Setmeal;
import com.wang.health.service.MemberService;
import com.wang.health.service.OrderInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wang.health.dao.OrderInfoMapper;
import com.wang.health.pojo.OrderInfo;
import com.wang.health.service.OrderSettingService;
import com.wang.health.service.SetmealService;
import com.wang.health.utils.CheckCodeUtil;
import com.wang.health.utils.EmptyUtil;
import com.wang.health.utils.SMSUtil;
import com.wang.health.utils.SnowflakeIdWorker;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@DubboService
public class OrderInfoServiceImpl extends ServiceImpl<OrderInfoMapper, OrderInfo> implements OrderInfoService {

    @Resource
    private MemberService memberService;

    @Resource
    private OrderInfoService orderInfoService;

    @Resource
    private OrderSettingService orderSettingService;

    @Resource
    private SetmealService setmealService;

    @Resource
    private SnowflakeIdWorker snowflakeIdWorker;

    @Override
    public String addOrderInfo(Map<String, String> map) {
        String phoneNumber = (String) map.get("telephone");
        String checkCode = (String) map.get("validateCode");
        Date orderDate = null;
        try {
            orderDate = new SimpleDateFormat("yyyy-MM-dd").parse(map.get("orderDate"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        //判断验证码是否正确
        checking(phoneNumber, checkCode);

        //1、查看该用户是否存在，如果该用户不存在就添加会员信息
        Member member = getMember(phoneNumber);
        if (member == null) {
            //如果该用户不存在就创建一个新的
            member = new Member();
            member.setName(map.get("name"));
            member.setTelephone(phoneNumber);
            member.setSex(map.get("sex"));
            member.setCreateDate(new Date());
            member.setIdCard(map.get("idCard"));
            memberService.save(member);
        } else {
            //如果该用户存在，就查询该用户当天是否预约过
            if (existOrder(orderDate, member)) {
                throw new RuntimeException("您今天已有预约，请勿重复预约");
            }
        }
        //3、查询当天的预约人数是否已经达到上限
        OrderSetting orderSetting = isFullForOrder(orderDate);
        //4、添加预约信息
        return doAddOrderInfo(map, orderDate, member, orderSetting);

    }

    private OrderSetting isFullForOrder(Date orderDate) {
        QueryWrapper<OrderSetting> orderSettingQueryWrapper = new QueryWrapper<>();
        orderSettingQueryWrapper.lambda().eq(OrderSetting::getOrderDate, orderDate);
        OrderSetting orderSetting = orderSettingService.getOne(orderSettingQueryWrapper);
        if (orderSetting == null) {
            throw new RuntimeException("该日期没有开通预约");
        }
        if (orderSetting.getNumber().equals(orderSetting.getReservations())) {

            throw new RuntimeException("该日期已被预约完，请另择预约日期");
        }
        return orderSetting;
    }

    private void checking(String phoneNumber, String checkCode) {
        String redisCheckCode = CheckCodeUtil.getCheckCode(phoneNumber);
        if (redisCheckCode == null) {
            throw new RuntimeException("没有获取验证码或者验证码已失效。");
        }
        if (!redisCheckCode.equals(checkCode)) {
            throw new RuntimeException("验证码不正确");
        }
    }

    private boolean existOrder(Date orderDate, Member member) {
        QueryWrapper<OrderInfo> orderInfoQueryWrapper = new QueryWrapper<>();
        orderInfoQueryWrapper.lambda()
                .eq(OrderInfo::getMemberId, member.getId())
                .eq(OrderInfo::getOrderDate, orderDate);
        return orderInfoService.getOne(orderInfoQueryWrapper) != null;
    }

    private Member getMember(String phoneNumber) {
        QueryWrapper<Member> memberQueryWrapper = new QueryWrapper<>();
        memberQueryWrapper.lambda().eq(Member::getTelephone, phoneNumber);
        return memberService.getOne(memberQueryWrapper);
    }

    private String doAddOrderInfo(Map<String, String> map, Date orderDate, Member member, OrderSetting orderSetting) {
        OrderInfo orderInfo = new OrderInfo();
        orderInfo.setMemberId(member.getId());
        orderInfo.setSetmealId(Long.parseLong(map.get("setmealId")));
        orderInfo.setOrderDate(orderDate);

        //雪花算法生成订单流水号
        orderInfo.setOrderNo(String.valueOf(snowflakeIdWorker.nextId()));

        this.save(orderInfo);
        orderSetting.setReservations(orderSetting.getReservations() + 1);
        orderSettingService.updateById(orderSetting);
        return orderInfo.getId() + "";
    }

    @Override
    public OrderInfoDto findOrderInfoById(String id) {
        //根据id查询预约信息
        OrderInfo orderInfo = this.getById(id);
        if (orderInfo == null) {
            throw new RuntimeException("查询结果不存在");
        }
        //根据会员id查询会员
        Member member = memberService.getById(orderInfo.getMemberId());
        //根据套餐id查询套餐
        Setmeal setmeal = setmealService.getById(orderInfo.getSetmealId());
        OrderInfoDto orderInfoDto = new OrderInfoDto();
        orderInfoDto.setMember(member.getName());
        orderInfoDto.setSetmeal(setmeal.getName());
        orderInfoDto.setOrderDate(new SimpleDateFormat("yyyy-MM-dd").format(orderInfo.getOrderDate()));
        orderInfoDto.setOrderType(orderInfo.getOrderType());
        //把oderNo回显
        orderInfoDto.setOrderNo(orderInfo.getOrderNo());
        return orderInfoDto;
    }

    /**
     * 订单支付状态的业务
     *
     * @param param
     */
    @Override
    public Boolean updatePayStatus(Map<String, String> param) {

        // 1.获取订单流水号
        String oid = param.get("out_trade_no");


        //2.根据订单流水号查询到订单信息（也可以自定义更新语句更新指定支付状态字段，这里为了使用updateWrapper）
        OrderInfo orderInfo = findOrderInfoByOrderNo(oid);

        //确定是否有该流水号的订单信息 判空 排除无效订单
        if(!ObjectUtils.isEmpty(orderInfo)){
            //3.设置更新订单支付状态 0未支付 1已支付 2支付失败
            orderInfo.setPayStatus(1);
        }

        //4.根据条件修改
        UpdateWrapper<OrderInfo> orderInfoUpdateWrapper = new UpdateWrapper<>();
        orderInfoUpdateWrapper.lambda().eq(OrderInfo::getOrderNo, oid);

         return update(orderInfo,orderInfoUpdateWrapper);
    }

    /**
     * 根据订单id查询订单信息
     * @param orderId
     * @return
     */
    public OrderInfo findOrderInfoByOrderId(String orderId) {
        return  getById(orderId);
    }

    /**
     * 根据订单流水号查询订单信息
     * @param orderNo
     * @return
     */
    public OrderInfo findOrderInfoByOrderNo(String orderNo) {
        return getOne(new QueryWrapper<OrderInfo>().lambda().eq(OrderInfo::getOrderNo,orderNo));
    }

}