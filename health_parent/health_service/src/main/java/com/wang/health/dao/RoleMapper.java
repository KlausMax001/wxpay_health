package com.wang.health.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wang.health.pojo.Role;

public interface RoleMapper extends BaseMapper<Role> {
}
