package com.wang.health.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wang.health.pojo.Role;
import com.wang.health.pojo.RoleUser;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface RoleUserMapper extends BaseMapper<RoleUser> {
    @Select("SELECT t_role.* FROM t_role,t_role_user WHERE t_role.id = t_role_user.role_id AND t_role_user.user_id = #{userId}")
    List<Role> findRoleByUserId(@Param("userId") String userId);
}
