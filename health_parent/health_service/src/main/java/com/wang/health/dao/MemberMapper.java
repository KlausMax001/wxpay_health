package com.wang.health.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wang.health.pojo.Member;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;
import java.util.Map;

public interface MemberMapper extends BaseMapper<Member> {
    @Select("SELECT \n" +
            "\tCOUNT(tb_member.`id`) value,\n" +
            "\tt.fmonth,\n" +
            "\tt.fyear \n" +
            "FROM \n" +
            "\t(SELECT * FROM rpt_date WHERE rpt_date.`id` BETWEEN #{startDate} AND #{endDate}) t \n" +
            "\tLEFT JOIN tb_member \n" +
            "\tON t.id = tb_member.`create_date_id` \n" +
            "GROUP BY \n" +
            "\tt.fmonth,\n" +
            "\tt.fyear\n" +
            "ORDER BY t.fyear ASC , t.fmonth ASC")
    List<Map> findByMonth(@Param("startDate") Long startDate,@Param("endDate") Long endDate);

}
