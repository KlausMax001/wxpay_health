package com.wang.health.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wang.health.pojo.CheckItem;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface CheckItemMapper extends BaseMapper<CheckItem> {

    @Select("SELECT t.* FROM tb_checkitem t,tb_group_item gt WHERE t.`id` = gt.`item_id`AND gt.`group_id` = #{checkGroupId}")
    List<CheckItem> findCheckItemsByCheckGroupId(@Param("checkGroupId") Long checkGroupId);
}
