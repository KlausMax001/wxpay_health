package com.wang.health.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wang.health.pojo.Permission;

public interface PermissionMapper extends BaseMapper<Permission> {
}
