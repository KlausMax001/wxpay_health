package com.wang.health.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Data
@TableName("tb_checkgroup")
public class CheckGroup implements Serializable {

    @TableId(value = "id",type = IdType.AUTO)
    private Long id;//ID
    @NotBlank(message = "编码不能为空")
    private String code;//编码
    @NotBlank(message = "名称不能为空")
    private String name;//名称
    private String sex;//适用性别(0-不限,1-男,2-女)
    private String helpCode;//助记码
    @Length(max = 100,message = "说明不能超过100个字")
    private String remark;//说明
    @Length(max = 100,message = "注意事项不能超过100个字")
    private String attention;//注意事项
    private String deleteFlag;//是否删除(0-未删除,1-删除)
}
