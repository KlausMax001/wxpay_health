package com.wang.health.pojo;

import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

@Data
@TableName("t_menu")
public class Menu implements Serializable {
	/**
	* ID
	*/
    @TableId(value="id",type = IdType.AUTO)
	private Long id;

	/**
	* 路径
	*/
	private String path;

	/**
	* 名称
	*/
	private String title;

	/**
	* 图标
	*/
	private String icon;

	/**
	* 父ID
	*/
	private Long pid;

	/**
	* 链接地址
	*/
	private String linkUrl;

	/**
	* 页面路径
	*/
	private String pagePath;


}