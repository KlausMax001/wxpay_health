package com.wang.health.service;

import com.wang.health.Do.UserDo;
import com.wang.health.pojo.User;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

public interface UserService extends IService<User> {

    public List<UserDo> userDoList();
}
