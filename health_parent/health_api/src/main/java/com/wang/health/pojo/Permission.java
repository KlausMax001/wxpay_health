package com.wang.health.pojo;

import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

@Data
@TableName("t_permission")
public class Permission implements Serializable {
	/**
	* ID
	*/
    @TableId(value="id",type = IdType.AUTO)
	private Long id;

	/**
	* 权限名称
	*/
	private String name;

	/**
	* 权限编码
	*/
	private String code;

	/**
	* 资源路径
	*/
	private String pathurl;

	/**
	* 上级ID
	*/
	private Long pid;


}