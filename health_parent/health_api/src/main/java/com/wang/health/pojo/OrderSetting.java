package com.wang.health.pojo;

import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Data
@TableName("tb_ordersetting")
public class OrderSetting implements Serializable {
	/**
	* ID
	*/
    @TableId(value="id",type = IdType.AUTO)
	private Long id;

	/**
	* 预约日期
	*/
	private Date orderDate;

	/**
	* 预约总数量
	*/
	private Integer number;

	/**
	* 已经预约数量
	*/
	private Integer reservations;

}