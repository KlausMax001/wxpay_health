package com.wang.health.service;

import com.wang.health.pojo.RolePermission;
import com.baomidou.mybatisplus.extension.service.IService;

public interface RolePermissionService extends IService<RolePermission> {

}
