package com.wang.health.service;

import com.wang.health.pojo.SetmealGroup;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

public interface SetmealGroupService extends IService<SetmealGroup> {

    void deleteBySetmealId(Long id);

    void addSetmealGroups(Long id, Long[] groupIds);

    List<Long> findBySetmealId(String id);
}
