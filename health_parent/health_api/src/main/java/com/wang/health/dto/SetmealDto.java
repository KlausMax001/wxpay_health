package com.wang.health.dto;

import com.wang.health.pojo.Setmeal;
import lombok.Data;

import java.util.List;

@Data
public class SetmealDto extends Setmeal {
    private List<CheckGroupDto> checkGroups;
}
