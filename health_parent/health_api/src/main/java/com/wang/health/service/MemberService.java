package com.wang.health.service;

import com.wang.health.pojo.Member;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface MemberService extends IService<Member> {

    Map findMemberByDate(List<Date> dateList);
}
