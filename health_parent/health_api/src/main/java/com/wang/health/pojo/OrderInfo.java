package com.wang.health.pojo;

import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

@Data
@TableName("tb_order")
public class OrderInfo implements Serializable {
	/**
	* ID
	*/
    @TableId(value="id",type = IdType.AUTO)
	private Long id;

	/**
	* 会员ID
	*/
	private Long memberId;

	/**
	* 套餐ID
	*/
	private Long setmealId;

	/**
	* 预约日期
	*/
	private Date orderDate;

	/**
	* 预约类型
	*/
	private String orderType;

	/**
	* 预约状态(1-预约成功,2-预约到诊)
	*/
	private Integer orderStatus;

	/**
	 * 订单流水id(雪花算法)
	 */
	private String orderNo;

	/**
	 * 订单支付状态(0未支付 1已支付 2支付失败)
	 */
	private Integer payStatus;

}