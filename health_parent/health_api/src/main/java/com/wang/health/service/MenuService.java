package com.wang.health.service;

import com.wang.health.pojo.Menu;
import com.baomidou.mybatisplus.extension.service.IService;

public interface MenuService extends IService<Menu> {

}
