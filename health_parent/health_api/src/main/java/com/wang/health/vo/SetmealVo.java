package com.wang.health.vo;

import com.wang.health.pojo.Setmeal;
import lombok.Data;

@Data
public class SetmealVo extends Setmeal {
    private Long[] groupIds;
}
