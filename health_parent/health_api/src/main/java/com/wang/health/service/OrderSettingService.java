package com.wang.health.service;

import com.wang.health.pojo.OrderSetting;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

public interface OrderSettingService extends IService<OrderSetting> {

    void importData(List<String[]> list) throws Exception;

    Map findByYearMonth(String yearMonth);

    Boolean updateByDate(String date, Integer number);

    //定时清理预约日期设置的历史垃圾
    boolean clearOrderDate();
}
