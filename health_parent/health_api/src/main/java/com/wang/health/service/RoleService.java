package com.wang.health.service;

import com.wang.health.pojo.Role;
import com.baomidou.mybatisplus.extension.service.IService;

public interface RoleService extends IService<Role> {

}
