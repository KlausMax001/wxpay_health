package com.wang.health.dto;

import lombok.Data;

import java.io.Serializable;


@Data
public class OrderInfoDto implements Serializable {
    private String member;//体检人姓名
    private String setmeal;//体检套餐
    private String orderDate;//预约体检日期
    private String orderType;//预约类型
    private Integer orderStatus;//订单状态
    private String orderNo;//订单流水号
}
