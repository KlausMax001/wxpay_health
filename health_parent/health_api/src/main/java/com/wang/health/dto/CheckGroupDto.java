package com.wang.health.dto;

import com.wang.health.pojo.CheckGroup;
import com.wang.health.pojo.CheckItem;
import lombok.Data;

import java.util.List;

@Data
public class CheckGroupDto extends CheckGroup {
    private List<CheckItem> checkItems;
}
