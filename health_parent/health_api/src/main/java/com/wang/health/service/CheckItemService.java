package com.wang.health.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wang.health.pojo.CheckItem;
import com.wang.health.vo.PageResult;
import com.wang.health.vo.QueryPageBean;

import java.util.List;

public interface CheckItemService extends IService<CheckItem> {
    PageResult findByPage(QueryPageBean pageBean);

    List<CheckItem> findCheckItemsByCheckGroupId(Long checkGroupId);
}
