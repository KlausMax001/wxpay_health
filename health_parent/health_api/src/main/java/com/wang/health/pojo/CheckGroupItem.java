package com.wang.health.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

@Data
@TableName("tb_group_item")
public class CheckGroupItem implements Serializable {

    @TableId(value = "id",type = IdType.AUTO)
    private Long id;//ID
    private Long itemId;//检查项id
    private Long groupId;//检查组id
}
