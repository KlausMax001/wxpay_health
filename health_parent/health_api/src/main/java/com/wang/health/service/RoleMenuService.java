package com.wang.health.service;

import com.wang.health.pojo.RoleMenu;
import com.baomidou.mybatisplus.extension.service.IService;

public interface RoleMenuService extends IService<RoleMenu> {

}
