package com.wang.health.vo;

import lombok.Data;

@Data
public class Result<T> {

    //自定义状态码，200表示成功，500表示服务器出异常。
    private Integer status = 200;

    //业务是否执行成功，true表示成功，false表示失败。
    private boolean flag = true;

    //执行结果需要返回的数据
    private T data;

    //错误信息
    private String errorMsg;

    public Result(T data){
        this.data = data;
    }
    public Result(boolean flag, String errorMsg){
        this.flag = flag;
        this.errorMsg = errorMsg;
        this.status = 500;
    }

    public static <T> Result<T> build(T data){
        return new Result<T>(data);
    }
    public static <T> Result<T> buildError(String errorMsg){
        return new Result<T>(false,errorMsg);
    }

}
