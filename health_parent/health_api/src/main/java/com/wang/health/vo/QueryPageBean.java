package com.wang.health.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class QueryPageBean implements Serializable {
    private Long currentPage;
    private Long pageSize;
    private String queryString;
}
