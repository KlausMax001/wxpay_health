package com.wang.health.Do;

import com.wang.health.pojo.Role;
import com.wang.health.pojo.User;
import lombok.Data;

import java.util.List;


@Data
public class UserDo extends User {

    private List<Role> roleList;
}
