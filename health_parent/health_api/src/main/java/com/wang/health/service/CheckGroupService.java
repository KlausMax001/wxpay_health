package com.wang.health.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wang.health.dto.CheckGroupDto;
import com.wang.health.pojo.CheckGroup;
import com.wang.health.vo.CheckGroupVo;
import com.wang.health.vo.PageResult;
import com.wang.health.vo.QueryPageBean;

public interface CheckGroupService extends IService<CheckGroup> {

    PageResult findByPage(QueryPageBean pageBean);

    boolean saveGroup(CheckGroupVo checkGroupVo);

    CheckGroupDto findCheckGroupDtoDtoById(Long checkGroupId);
}
