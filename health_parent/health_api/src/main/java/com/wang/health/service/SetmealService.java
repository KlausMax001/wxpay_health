package com.wang.health.service;

import com.wang.health.dto.SetmealDto;
import com.wang.health.pojo.Setmeal;
import com.baomidou.mybatisplus.extension.service.IService;
import com.wang.health.vo.PageResult;
import com.wang.health.vo.QueryPageBean;
import com.wang.health.vo.SetmealVo;

import java.util.List;
import java.util.Map;

public interface SetmealService extends IService<Setmeal> {

    boolean saveSetmeal(SetmealVo setmealVo);

    PageResult findByPage(QueryPageBean pageBean);

    boolean clearOssPic();

    SetmealDto findSetmealDtoById(Long setmealId);

    List<Map> getSetmealReport();
}
