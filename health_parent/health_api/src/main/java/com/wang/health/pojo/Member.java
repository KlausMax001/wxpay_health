package com.wang.health.pojo;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

@Data
@TableName("tb_member")
public class Member implements Serializable {
	/**
	* ID
	*/
    @TableId(value="id",type = IdType.AUTO)
	private Long id;

	/**
	* 体检人
	*/
	private String name;

	/**
	* 性别
	*/
	private String sex;

	/**
	* 手机号
	*/
	private String telephone;

	/**
	* 身份证号
	*/
	private String idCard;

	/**
	* 创建日期
	*/
	private Date createDate;
	/**
	 * 创建日期id
	 */
	private String createDateId;

	public String getCreateDateId(){
		if (this.createDate==null){
			return null;
		}
		String format = new SimpleDateFormat("yyyy-MM-dd").format(this.createDate);
		return format.replaceAll("-","");
	}

}