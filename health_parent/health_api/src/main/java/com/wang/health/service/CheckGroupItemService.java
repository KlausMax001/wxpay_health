package com.wang.health.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wang.health.pojo.CheckGroupItem;

import java.util.List;

public interface CheckGroupItemService extends IService<CheckGroupItem> {
    void deleteByGroupId(Long id);

    void addGroupItemsByItemIds(Long id, Long[] itemIds);

    List<Long> findByGroupId(String groupId);
}
