package com.wang.health.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;

@Data
@TableName("tb_checkitem")
public class CheckItem implements Serializable {
    @TableId(value = "id",type = IdType.AUTO)
    private Long id;//ID
    @NotBlank(message = "项目编码不能为空")
    private String code;//项目编码
    @NotBlank(message = "项目名称不能为空")
    private String name;//项目名称
    private String sex;//适用性别(0-不限,1-男,2-女)
    private String age;//适用年龄
    private String type;//类型(1-检查,2-检验)
    private BigDecimal price;//价格
    @Length(max = 100,message = "项目说明不能超过100个字")
    private String remark;//项目说明
    @Length(max = 100,message = "注意事项不能超过100个字")
    private String attention;//注意事项
    private String deleteFlag;//是否删除，0-未删除，1-删除。
}
