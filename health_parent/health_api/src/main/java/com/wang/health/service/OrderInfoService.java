package com.wang.health.service;

import com.wang.health.dto.OrderInfoDto;
import com.wang.health.pojo.OrderInfo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

public interface OrderInfoService extends IService<OrderInfo> {


    String addOrderInfo(Map<String,String> map);

    OrderInfoDto findOrderInfoById(String id);

    //更新支付状态
    Boolean updatePayStatus(Map<String,String> param);

    //根据订单号查询订单表信息
    OrderInfo findOrderInfoByOrderId(String orderId);

    //根据订单流水id查询订单表信息
    OrderInfo findOrderInfoByOrderNo(String orderNo);
}
