package com.wang.health.service;

import com.wang.health.pojo.Role;
import com.wang.health.pojo.RoleUser;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

public interface RoleUserService extends IService<RoleUser> {

    public List<Role> findRoleByUserId(String userId);
}
