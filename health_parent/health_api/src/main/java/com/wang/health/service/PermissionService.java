package com.wang.health.service;

import com.wang.health.pojo.Permission;
import com.baomidou.mybatisplus.extension.service.IService;

public interface PermissionService extends IService<Permission> {

}
