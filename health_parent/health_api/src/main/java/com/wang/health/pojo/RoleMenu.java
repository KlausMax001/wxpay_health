package com.wang.health.pojo;

import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

@Data
@TableName("t_role_menu")
public class RoleMenu implements Serializable {
	/**
	* ID
	*/
    @TableId(value="id",type = IdType.AUTO)
	private Long id;

	/**
	* 角色ID
	*/
	private Long roleId;

	/**
	* 菜单ID
	*/
	private Long menuId;


}