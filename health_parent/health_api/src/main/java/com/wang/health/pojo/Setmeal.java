package com.wang.health.pojo;

import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Data
@TableName("tb_setmeal")
public class Setmeal implements Serializable {
	/**
	* ID
	*/
    @TableId(value="id",type = IdType.AUTO)
	private Long id;

	/**
	* 套餐编号
	*/
	@NotBlank(message = "套餐编号不能为空")
	private String code;

	/**
	* 名称
	*/
	@NotBlank(message = "套餐名称不能为空")
	private String name;

	/**
	* 适用性别
	*/
	private String sex;

	/**
	* 助记码
	*/
	private String helpCode;

	/**
	* 套餐价格
	*/
	private BigDecimal price;

	/**
	* 适用年龄
	*/
	private String age;

	/**
	* 上传图片
	*/
	private String img;

	/**
	* 说明
	*/
	@Length(max = 100,message = "套餐说明不能超过100个字")
	private String remark;

	/**
	* 注意事项
	*/
	@Length(max = 100,message = "注意事项不能超过100个字")
	private String attention;

	/**
	* 是否删除
	*/
	private Integer deleteFlag;


}