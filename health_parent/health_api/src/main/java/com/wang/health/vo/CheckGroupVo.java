package com.wang.health.vo;

import com.wang.health.pojo.CheckGroup;
import lombok.Data;

@Data
public class CheckGroupVo extends CheckGroup {
    private Long[] itemIds;
}
